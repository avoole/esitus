package m2ng;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class BossmanBullets extends GameObject{
	
	private Handler handler;
	Random r = new Random();

	public BossmanBullets(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		
		this.handler = handler;
		
		velX = (r.nextInt(5 - -5) + -5);
		velY = 5;
		
	}
	
	public Rectangle getBounds(){
		return new Rectangle((int)x, (int)y, 16, 16);

	}
	
	public void tick() {
		x += velX;
		y += velY;
		
		if(y <= 0 || y >= Javaprojekt.HEIGHT - 32) velY *= -1;
		if(x <= 0 || x >= Javaprojekt.WIDTH - 16) velX *= -1;
		
		if(y >= Javaprojekt.HEIGHT)handler.removeObject(this);
		
		handler.addObject(new Rada(x, y, ID.Trail, Color.red, 16, 16, 0.02f, handler));
		
		
		
		
	}

	
	public void render(Graphics g) {
			g.setColor(Color.red);
			g.fillRect((int)x, (int)y, 16, 16);
	}

}
