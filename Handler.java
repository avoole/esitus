package m2ng;

import java.awt.Graphics;
import java.util.LinkedList;

public class Handler {
	
	//Objektide juhtimine
	
	LinkedList<GameObject> object = new LinkedList<GameObject>();
	
	public void tick(){
		for(int i = 0; i < object.size(); i++){
			GameObject tempObject = object.get(i);
			
			tempObject.tick();
		}
		
	}
	
	public void render(Graphics g){
		for(int i = 0; i < object.size(); i++){
			GameObject tempObject = object.get(i);
			
			tempObject.render(g);
		}
		
	}
	
	public void clearEnemys(){
		for(int i = 0; i < object.size(); i++){
			GameObject tempObject = object.get(i);
			
			if(tempObject.getId() != ID.M2ngija)
			{
				object.clear();
				addObject(new M2ngija((int)tempObject.getX(), (int)tempObject.getY(), ID.M2ngija, this));
			}
			
		}
			
		
	}
	
	public void addObject(GameObject object){
		this.object.add(object);
	}
	
	public void removeObject(GameObject object){
		this.object.remove(object);
	}
	
	
}
