package m2ng;

public enum ID {
	
	M2ngija(),
	NewEnemy(),
	WittyEnemy(),
	Bossman(),
	BossmanBullet(), 
	Trail(),
	Enemy();

}
