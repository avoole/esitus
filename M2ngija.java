package m2ng;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Random;

public class M2ngija extends GameObject {
	
	Random r = new Random ();
	Handler handler;

	public M2ngija(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
	
	}
	
	public Rectangle getBounds(){
		return new Rectangle((int)x, (int)y, 32, 32);
	}

	public void tick() {
		x += velX;
		y += velY;
		
		x = Javaprojekt.clamp((int)x, 0, Javaprojekt.WIDTH -37);
		y = Javaprojekt.clamp(y, 0, Javaprojekt.HEIGHT -60);
		
		handler.addObject(new Rada(x, y, ID.Trail, Color.white, 32, 32, 0.06f, handler));
		
		collision();
		
		
	}
	
	private void collision(){
		for(int i = 0; i < handler.object.size(); i++){
			
			GameObject tempObject = handler.object.get(i);
			
			if(tempObject.getId() == ID.Enemy || tempObject.getId() == ID.NewEnemy || tempObject.getId() == ID.WittyEnemy) {
				
				if(getBounds().intersects(tempObject.getBounds())){
					
					HUD.HEALTH -= 2;
				}
			}
			
			
		}
	
	}

	public void render(Graphics g) {
		
		g.setColor(Color.white);
		g.fillRect((int)x, (int)y, 32, 32);
	
	}
	
	

}
