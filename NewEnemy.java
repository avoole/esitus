package m2ng;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class NewEnemy extends GameObject{
	
	private Handler handler;

	public NewEnemy(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		
		this.handler = handler;
		
		velX = 2;
		velY = 9;
		
	}
	
	public Rectangle getBounds(){
		return new Rectangle((int)x, (int)y, 16, 16);

	}
	
	public void tick() {
		x += velX;
		y += velY;
		
		if(y <= 0 || y >= Javaprojekt.HEIGHT - 32) velY *= -1;
		if(x <= 0 || x >= Javaprojekt.WIDTH - 16) velX *= -1;
		
		handler.addObject(new Rada(x, y, ID.Trail, Color.orange, 16, 16, 0.02f, handler));
		
		
		
		
	}

	
	public void render(Graphics g) {
			g.setColor(Color.orange);
			g.fillRect((int)x, (int)y, 16, 16);
	}

}
