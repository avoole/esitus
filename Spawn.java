package m2ng;

import java.awt.Color;
import java.util.Random;

public class Spawn {
	
	private Handler handler;
	private HUD hud;
	private Random r = new Random ();
	
	private int scoreKeep = 0;
	
	public Spawn(Handler handler, HUD hud){
		this.handler = handler;
		this.hud = hud;
		
	}
	
	public void tick(){
		scoreKeep++; 
		
		if(scoreKeep >= 250){
			scoreKeep = 0;
			hud.setLevel(hud.getLevel() + 1);
			
			if(hud.getLevel()== 2){
				handler.addObject(new Enemy(r.nextInt(Javaprojekt.WIDTH - 50), r.nextInt(Javaprojekt.HEIGHT - 50), ID.Enemy, handler));
			}else if(hud.getLevel() == 3){
				handler.addObject(new Enemy(r.nextInt(Javaprojekt.WIDTH - 50), r.nextInt(Javaprojekt.HEIGHT - 50), ID.Enemy, handler));
			}else if(hud.getLevel() == 4){
				handler.addObject(new NewEnemy(r.nextInt(Javaprojekt.WIDTH - 50), r.nextInt(Javaprojekt.HEIGHT - 50), ID.NewEnemy, handler));
			}else if(hud.getLevel() == 5){
				handler.addObject(new WittyEnemy(r.nextInt(Javaprojekt.WIDTH - 50), r.nextInt(Javaprojekt.HEIGHT - 50), ID.WittyEnemy, handler));
			}else if(hud.getLevel() == 6){
				handler.addObject(new NewEnemy(r.nextInt(Javaprojekt.WIDTH - 50), r.nextInt(Javaprojekt.HEIGHT - 50), ID.NewEnemy, handler));
			}else if(hud.getLevel() == 8){
				handler.clearEnemys();
				handler.addObject(new Bossman((Javaprojekt.WIDTH /2) -48 , -120, ID.Bossman, handler));
			}
		
			
		}
		
		
	}
	
	
}
